from fastapi import APIRouter, Depends, Response, HTTPException
from typing import Union, List
from queries.reviews import (
    ReviewIn,
    ReviewRepository,
    ReviewOut,
    ReviewOutWithFirstName,
    Error,
)

# Addons #
router = APIRouter()
repository = ReviewRepository()


# POST #
@router.post("/reviews", response_model=Union[ReviewOut, Error])
def create_review(
    review: ReviewIn, response: Response, repo: ReviewRepository = Depends()
):
    created_review = repo.create(review)
    if isinstance(created_review, ReviewOut):
        response.status_code = 201
    else:
        response.status_code = 400
    return created_review


# GETS #
@router.get("/reviews", response_model=Union[Error, List[ReviewOut]])
def get_reviews(repo: ReviewRepository = Depends()):
    return repo.get_all()


@router.get("/reviews/{review_id}", response_model=ReviewOut)
def get_review(review_id: int):
    review = repository.get_review(review_id)
    if isinstance(review, Error):
        raise HTTPException(status_code=404, detail=review.message)
    return review


# DELETE #
@router.delete("/reviews/{review_id}", response_model=None)
def delete_review(review_id: int) -> Union[None, HTTPException]:
    deleted_review = repository.delete(review_id)
    if isinstance(deleted_review, Error):
        raise HTTPException(status_code=404, detail=deleted_review.message)
    return None


# PUT #
@router.put("/reviews/{review_id}", response_model=Union[ReviewOut, Error])
def update_review(
    review_id: int, review: ReviewIn, repo: ReviewRepository = Depends()
):
    updated_review = repo.update_review(review_id, review)
    if isinstance(updated_review, Error):
        raise HTTPException(status_code=404, detail=updated_review.message)
    return updated_review


@router.get(
    "/reviews_by_profile/{profile_id}",
    response_model=List[ReviewOutWithFirstName],
)
def get_reviews_by_reviewee_profile(
    profile_id: int,
    repo: ReviewRepository = Depends(),
) -> List[ReviewOutWithFirstName]:
    reviews = repo.get_reviews_by_reviewee_profile(profile_id)
    # print(f">>>>>> reviews: {reviews}")
    return reviews
