from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool
from datetime import datetime


# Models #
class Error(BaseModel):
    message: str


class MessageIn(BaseModel):
    sender: int
    receiver: int
    date: datetime
    subject: str
    body: str


class MessageOut(BaseModel):
    message_id: int
    sender: int
    receiver: int
    date: datetime
    subject: str
    body: str


# Repo #
class MessageRepository:
    def delete(self, message_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM message
                        WHERE message_id = %s
                        """,
                        [message_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def update(
        self, message_id: int, message: MessageIn
    ) -> Union[MessageOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE message
                        SET sender = %s,
                        receiver = %s,
                        date = %s,
                        subject = %s,
                        body = %s

                        WHERE message_id = %s
                        """,
                        [
                            message.sender,
                            message.receiver,
                            message.date,
                            message.subject,
                            message.body,
                            message_id,
                        ],
                    )
                    data = message.dict()
                    return MessageOut(message_id=message_id, **data)
        except Exception as e:
            print(e)
            return {
                "message": "Update Failed, Check the input Data and try again."
            }

    def get_all(self) -> Union[Error, List[MessageOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                                SELECT message_id,
                                sender,
                                receiver,
                                date,
                                subject,
                                body

                                FROM message
                                ORDER BY message_id;
                                """
                    )
                    result = []
                    for record in db:
                        message = MessageOut(
                            message_id=record[0],
                            sender=record[1],
                            receiver=record[2],
                            date=record[3],
                            subject=record[4],
                            body=record[5],
                        )
                        result.append(message)
                    return result

        except Exception as e:
            print(e)
            return {"message": "Failed to retrieve messages"}

    def get_one(self, message_id: int) -> Optional[MessageOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """SELECT message_id,
                            sender,
                            receiver,
                            date,
                            subject,
                            body

                            FROM message
                            WHERE message_id = %s
                            """,
                        [message_id],
                    )
                    record = result.fetchone()
                    return MessageOut(
                        message_id=record[0],
                        sender=record[1],
                        receiver=record[2],
                        date=record[3],
                        subject=record[4],
                        body=record[5],
                    )
        except Exception as e:
            print(e)
            return {"message": "Sorry Couldnt Get that Message"}

    def create(self, message: MessageIn) -> MessageOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                            INSERT INTO message
                            (
                                sender,
                                receiver,
                                date,
                                subject,
                                body
                            )
                            VALUES
                                (%s, %s, %s, %s, %s)
                            RETURNING message_id;
                            """,
                        [
                            message.sender,
                            message.receiver,
                            message.date,
                            message.subject,
                            message.body,
                        ],
                    )

                    message_id = result.fetchone()[0]
                    data = message.dict()
                    return MessageOut(message_id=message_id, **data)

        except Exception as e:
            print(e)
            return {
                "message": "Looks like something went wrong. \
                  Check the input Data and try again."
            }

    def get_received_messages_by_profile(
        self, profile_id: int
    ) -> List[MessageOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        message_id,
                        sender,
                        receiver,
                        date,
                        subject,
                        body
                        FROM message
                        WHERE receiver = %s
                        ORDER BY date;
                        """,
                        [profile_id],
                    )
                    messages = []
                    for record in db:
                        message = MessageOut(
                            message_id=record[0],
                            sender=record[1],
                            receiver=record[2],
                            date=record[3],
                            subject=record[4],
                            body=record[5],
                        )
                        messages.append(message)
                    return messages
        except Exception as e:
            print(e)
            return {"message": "Error while getting received messages."}

    def get_sent_messages_by_profile(
        self, profile_id: int
    ) -> List[MessageOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        message_id,
                        sender,
                        receiver,
                        date,
                        subject,
                        body
                        FROM message
                        WHERE sender = %s
                        ORDER BY date;
                        """,
                        [profile_id],
                    )
                    messages = []
                    for record in db:
                        message = MessageOut(
                            message_id=record[0],
                            sender=record[1],
                            receiver=record[2],
                            date=record[3],
                            subject=record[4],
                            body=record[5],
                        )
                        messages.append(message)
                    return messages
        except Exception as e:
            print(e)
            return {"message": "Error while trying to get sent messages"}
