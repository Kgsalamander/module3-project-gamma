from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


# Models #
class Error(BaseModel):
    message: str


class ProfileIn(BaseModel):
    account: int
    picture_url: Optional[str]
    first_name: str
    last_name: str
    skills: Optional[str]
    interests: Optional[str]
    bio: Optional[str]


class ProfileOut(BaseModel):
    profile_id: int
    account: int
    picture_url: Optional[str]
    first_name: str
    last_name: str
    skills: Optional[str]
    interests: Optional[str]
    bio: Optional[str]


# Repo #
class ProfileRepository:
    def delete(self, profile_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM profile
                        WHERE profile_id = %s
                        """,
                        [profile_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def update(
        self, profile_id: int, profile: ProfileIn
    ) -> Union[ProfileOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE profile
                        SET account = %s,
                        picture_url = %s,
                        first_name = %s,
                        last_name = %s,
                        skills = %s,
                        interests = %s,
                        bio = %s
                        WHERE profile_id = %s
                        """,
                        [
                            profile.account,
                            profile.picture_url,
                            profile.first_name,
                            profile.last_name,
                            profile.skills,
                            profile.interests,
                            profile.bio,
                            profile_id,
                        ],
                    )
                    data = profile.dict()
                    return ProfileOut(profile_id=profile_id, **data)
        except Exception as e:
            print(e)
            return {
                "message": "Update Failed, Check the input Data and try again."
            }

    def get_all(self) -> Union[Error, List[ProfileOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT profile_id,
                        account,
                        picture_url,
                        first_name,
                        last_name,
                        skills,
                        interests,
                        bio
                        FROM profile
                        ORDER BY profile_id;
                        """
                    )
                    results = []
                    for record in result:
                        profile = ProfileOut(
                            profile_id=record[0],
                            account=record[1],
                            picture_url=record[2],
                            first_name=record[3],
                            last_name=record[4],
                            skills=record[5],
                            interests=record[6],
                            bio=record[7],
                        )
                        results.append(profile)
                    return results
        except Exception as e:
            print(e)
            return {"message": "Failed to retrieve profiles"}

    def get_profile_by_account(self, account: int) -> Optional[ProfileOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        profile_id,
                        account,
                        picture_url,
                        first_name,
                        last_name,
                        skills,
                        interests,
                        bio
                        FROM profile
                        WHERE account = %s
                        """,
                        [account],
                    )
                    record = result.fetchone()
                    if record:
                        return ProfileOut(
                            profile_id=record[0],
                            account=record[1],
                            picture_url=record[2],
                            first_name=record[3],
                            last_name=record[4],
                            skills=record[5],
                            interests=record[6],
                            bio=record[7],
                        )
                    else:
                        return None
        except Exception as e:
            print(e)
            return {"message": "Sorry, couldn't retrieve that Profile"}

    def get_one(self, profile_id: int) -> Optional[ProfileOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        profile_id,
                        account,
                        picture_url,
                        first_name,
                        last_name,
                        skills,
                        interests,
                        bio
                        FROM profile
                        WHERE profile_id = %s
                        """,
                        [profile_id],
                    )
                    record = result.fetchone()
                    if record:
                        return ProfileOut(
                            profile_id=record[0],
                            account=record[1],
                            picture_url=record[2],
                            first_name=record[3],
                            last_name=record[4],
                            skills=record[5],
                            interests=record[6],
                            bio=record[7],
                        )
                    else:
                        return None
        except Exception as e:
            print(e)
            return {"message": "Sorry, couldn't retrieve that Profile"}

    def create(self, profile: ProfileIn) -> ProfileOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO profile
                        (
                          account,
                          picture_url,
                          first_name,
                          last_name,
                          skills,
                          interests,
                          bio
                        )
                        VALUES (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING profile_id;
                        """,
                        [
                            profile.account,
                            profile.picture_url,
                            profile.first_name,
                            profile.last_name,
                            profile.skills,
                            profile.interests,
                            profile.bio,
                        ],
                    )

                    new_id = result.fetchone()[0]
                    data = profile.dict()
                    return ProfileOut(profile_id=new_id, **data)

        except Exception as e:
            print(e)
            return {
                "message": "An error occurred."
            }
