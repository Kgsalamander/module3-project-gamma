from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool
from datetime import datetime


# Models #
class Error(BaseModel):
    message: str


class ReviewIn(BaseModel):
    author: int
    reviewee: int
    date: datetime
    rating: int
    review_text: Optional[str]


class ReviewOut(BaseModel):
    review_id: int
    author: int
    reviewee: int
    date: datetime
    rating: int
    review_text: Optional[str]


class ReviewOutWithFirstName(BaseModel):
    review_id: int
    author: str
    reviewee: int
    date: datetime
    rating: int
    review_text: Optional[str]


# Repo #
class ReviewRepository:
    def get_all(self) -> Union[Error, List[ReviewOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        review_id,
                        author,
                        reviewee,
                        date,
                        rating,
                        review_text
                        FROM review
                        ORDER BY review_id;
                        """
                    )
                    result = []
                    for record in db:
                        review = ReviewOut(
                            review_id=record[0],
                            author=record[1],
                            reviewee=record[2],
                            date=record[3],
                            rating=record[4],
                            review_text=record[5],
                        )
                        result.append(review)
                    return result
        except Exception as e:
            print(e)
            return {"message": "An error occurred while getting all reviews."}

    def create(self, review: ReviewIn) -> ReviewOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO review
                        (
                            author,
                            reviewee,
                            date,
                            rating,
                            review_text
                        )
                        VALUES
                            (%s, %s, %s, %s, %s)
                        RETURNING review_id;
                        """,
                        [
                            review.author,
                            review.reviewee,
                            review.date,
                            review.rating,
                            review.review_text,
                        ],
                    )

                    review_id = result.fetchone()[0]
                    data = review.dict()
                    return ReviewOut(review_id=review_id, **data)
        except Exception as e:
            print(e)
            return {"message": "An error occurred while creating this review."}

    def delete(self, review_id: int) -> Union[None, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        DELETE FROM review
                        WHERE review_id = %s
                        RETURNING review_id;
                        """,
                        [review_id],
                    )
                    deleted_id = result.fetchone()
                    if not deleted_id:
                        return Error(message="Review does not exist")
                    return None
        except Exception as e:
            print(e)
            return {
                "message": "An error occurred while trying to delete review."
            }

    def get_review(self, review_id: int) -> Union[ReviewOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        review_id,
                        author,
                        reviewee,
                        date,
                        rating,
                        review_text
                        FROM review
                        WHERE review_id = %s;
                        """,
                        [review_id],
                    )
                    record = result.fetchone()
                    if not record:
                        return Error(message="Review does not exist")

                    review = ReviewOut(
                        review_id=record[0],
                        author=record[1],
                        reviewee=record[2],
                        date=record[3],
                        rating=record[4],
                        review_text=record[5],
                    )
                    return review
        except Exception as e:
            print(e)
            return {"message": "An error occurred while trying to get review."}

    def update_review(
        self, review_id: int, review: ReviewIn
    ) -> Union[ReviewOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE review
                        SET
                            author = %s,
                            reviewee = %s,
                            date = %s,
                            rating = %s,
                            review_text = %s
                        WHERE review_id = %s
                        RETURNING
                            review_id,
                            author,
                            reviewee,
                            date,
                            rating,
                            review_text;
                        """,
                        [
                            review.author,
                            review.reviewee,
                            review.date,
                            review.rating,
                            review.review_text,
                            review_id,
                        ],
                    )

                    updated_review_data = result.fetchone()
                    if updated_review_data:
                        return ReviewOut(
                            review_id=updated_review_data[0],
                            author=updated_review_data[1],
                            reviewee=updated_review_data[2],
                            date=updated_review_data[3],
                            rating=updated_review_data[4],
                            review_text=updated_review_data[5],
                        )
                    else:
                        return Error(
                            message=f"Review {review_id} does not exist."
                        )
        except Exception as e:
            print(e)
            return {
                "message": "An error occurred while trying to update review."
            }

    def get_reviews_by_reviewee_profile(
        self, profile_id: int
    ) -> List[ReviewOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        r.review_id,
                        p.first_name as author,
                        r.reviewee,
                        r.date,
                        r.rating,
                        r.review_text
                        FROM review r
                        JOIN profile p ON r.author = p.profile_id
                        WHERE reviewee = %s
                        ORDER BY r.date;
                        """,
                        [profile_id],
                    )
                    reviews = []
                    for record in db:
                        review = ReviewOutWithFirstName(
                            review_id=record[0],
                            author=record[1],
                            reviewee=record[2],
                            date=record[3],
                            rating=record[4],
                            review_text=record[5],
                        )
                        reviews.append(review)
                    return reviews
        except Exception as e:
            print(e)
            return {
                "message": "An error occurred retrieving reviews."
            }
