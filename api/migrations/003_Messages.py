# Create the table
steps = [
    [
        # CREATE TABLE
        """
        CREATE TABLE message (
            message_id SERIAL PRIMARY KEY NOT NULL,
            sender INT NOT NULL REFERENCES profile(profile_id) ON DELETE CASCADE,
            receiver INT NOT NULL REFERENCES profile(profile_id) ON DELETE CASCADE,
            date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
            subject VARCHAR(255) NOT NULL,
            body TEXT NOT NULL
        );
        """,
        # DROP TABLE
        """
        DROP TABLE message;
        """
    ]
]
