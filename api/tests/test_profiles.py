from fastapi.testclient import TestClient
from main import app
from queries.profiles import ProfileRepository

client = TestClient(app)

# Freddys Test


class EmptyProfileRepository:
    def get_all(self):
        return [
            {
                "profile_id": 1,
                "account": 1,
                "picture_url": "https://example.com/profile.jpg",
                "first_name": "John",
                "last_name": "Doe",
                "skills": "Python, JavaScript",
                "interests": "Reading, Coding",
                "bio": "Software Engineer",
            }
        ]


def test_get_all_profiles():
    app.dependency_overrides[ProfileRepository] = EmptyProfileRepository
    response = client.get("/profiles")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0]["profile_id"] == 1
    assert response.json()[0]["first_name"] == "John"
