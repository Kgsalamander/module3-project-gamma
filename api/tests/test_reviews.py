from fastapi.testclient import TestClient
from main import app
from queries.reviews import ReviewRepository

client = TestClient(app)


class EmptyReviewRepository:
    def get_all(self):
        return [
            {
                "review_id": 1,
                "author": 1,
                "reviewee": 2,
                "date": "2023-09-01T12:00:00Z",
                "rating": 5,
                "review_text": "Great experience",
            }
        ]


def test_get_all_reviews():
    app.dependency_overrides[ReviewRepository] = EmptyReviewRepository
    response = client.get("/reviews")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0]["review_id"] == 1
    assert response.json()[0]["rating"] == 5
