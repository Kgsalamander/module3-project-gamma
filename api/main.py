import os
from psycopg_pool import ConnectionPool
from authenticator import MyAuthenticator
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI
from routers import profiles, reviews, events, accounts, messages


# Addons #
pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))
authenticator = MyAuthenticator(os.environ["SIGNING_KEY"])


# API Routers #
app = FastAPI()
app.include_router(accounts.router)
app.include_router(profiles.router)
app.include_router(events.router)
app.include_router(reviews.router)
app.include_router(messages.router)
app.include_router(authenticator.router)


@app.get("/")
def root():
    return {"message": "We've hit the root path;)"}


# MiddleWare #
origins = [
    "https://team2ter.gitlab.io/2ter",
    os.environ.get("CORS_HOST", None),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# GETS #
@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }
