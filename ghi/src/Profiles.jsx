import React, { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { useContextStore } from "./ContextStore";
import { Link as RouterLink } from "react-router-dom";
import {
  Typography,
  Box,
  Grid,
  Link,
  Button,
  Switch,
  Container,
} from "@mui/material";

function Profiles() {
  const {
    myProfileDict,
    profilesDict,
    getProfiles,
    setViewProfileDict,
    setViewProfileId,
    setReviewee,
  } = useContextStore();

  useEffect(() => {
    getProfiles();
    setReviewee(myProfileDict.profile_id);
  }, []);

  const handleLinkClick = (profile) => {
    setViewProfileDict(profile);
    setViewProfileId(profile.profile_id);
    setReviewee(profile.profile_id);
  };

  const label = { inputProps: { "aria-label": "messages toggle" } };
  const [toggleLeft, setToggleLeft] = useState(true);
  const [searchQuery, setSearchQuery] = useState("");
  const [filteredProfiles, setFilteredProfiles] = useState([]);
  const [searchActive, setSearchActive] = useState(false);

  const handleToggle = () => {
    setToggleLeft(!toggleLeft);
  };

  const handleClearSearch = async () => {
    setSearchActive(false);
    setSearchQuery("");
  };

  const handleSearchClick = async () => {
    setSearchActive(true);
    toggleLeft
      ? setFilteredProfiles(
          profilesDict.filter((profile) => profile.skills.includes(searchQuery))
        )
      : setFilteredProfiles(
          profilesDict.filter((profile) =>
            profile.interests.includes(searchQuery)
          )
        );
  };

  return (
    <Container>
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <Typography variant="h1" sx={{ marginBottom: 2 }}>
        Profiles
      </Typography>
      <h4>Search toggle</h4>
      <Box>
        Skills
        <Switch {...label} color="default" onChange={handleToggle} />
        Interests
      </Box>
      <header>
        <h1 className="navbar-brand">Search</h1>
        <form className="d-flex">
          <input
            className="form-control me-2"
            type="search"
            placeholder="Search interest"
            aria-label="Search"
            maxLength={100}
            value={searchQuery}
            onChange={(event) => setSearchQuery(event.target.value)}
            onKeyPress={(event) => {
              if (event.key === "Enter") {
                event.preventDefault();
                handleSearchClick();
              }
            }}
          />
          <Button
            size="small"
            variant="contained"
            color="secondary"
            onClick={() => handleSearchClick()}
            sx={{ margin: 1, fontSize: "0.7rem" }}
          >
            Search
          </Button>
          <Button
            size="small"
            variant="contained"
            color="secondary"
            onClick={() => handleClearSearch()}
            sx={{ margin: 1, fontSize: "0.7rem" }}
          >
            Clear Search
          </Button>
        </form>
      </header>
      <Grid container spacing={2}>
        {searchActive
          ? filteredProfiles?.map((profile) => (
              <Grid
                sx={{ margin: "1rem" }}
                key={profile.profile_id}
                item
                xs={12}
                sm={6}
                md={3}
                xl={3}
                height="600px"
              >
                <Box height="600px">
                  <RouterLink to="/view_profile">
                    <Link
                      variant="body2"
                      onClick={() => handleLinkClick(profile)}
                      component="div"
                      sx={{
                        textDecoration: "none",
                        "&:hover": {
                          textDecoration: "none",
                        },
                      }}
                      height="100%"
                    >
                      <Box className="card" sx={{ height: "100%" }}>
                        <Typography variant="h5">
                          {profile.first_name} {profile.last_name}
                        </Typography>
                        <Typography variant="h5">Skills</Typography>
                        <Typography>{profile.skills}</Typography>
                        <Typography variant="h5">Interests</Typography>
                        <Typography>{profile.interests}</Typography>
                        <Typography variant="h5">Bio</Typography>
                        <Typography sx={{
                            overflow: 'scroll',
                            textOverflow: 'ellipsis',
                            display: '-webkit-box',
                            WebkitBoxOrient: 'vertical',
                        }}>{profile.bio}</Typography>
                      </Box>
                    </Link>
                  </RouterLink>
                </Box>
              </Grid>
            ))
          : profilesDict?.map((profile) => (
              <Grid
                sx={{ margin: "1rem" }}
                key={profile.profile_id}
                item
                xs={12}
                sm={6}
                md={3}
                xl={3}
                height="600px"
              >
                <Box height="600px">
                  <RouterLink to="/view_profile">
                    <Link
                      variant="body2"
                      onClick={() => handleLinkClick(profile)}
                      component="div"
                      sx={{
                        textDecoration: "none",
                        "&:hover": {
                          textDecoration: "none",
                        },
                      }}
                      height="100%"
                    >
                      <Box className="card" sx={{ height: "100%", maxHeight: "100%" }}>
                        <Typography variant="h5">
                          {profile.first_name} {profile.last_name}
                        </Typography>
                        <Typography variant="h5">Skills</Typography>
                        <Typography>{profile.skills}</Typography>
                        <Typography variant="h5">Interests</Typography>
                        <Typography>{profile.interests}</Typography>
                        <Typography variant="h5">Bio</Typography>
                        <Typography sx={{
                            overflow: 'scroll',
                            textOverflow: 'ellipsis',
                            display: '-webkit-box',
                            WebkitBoxOrient: 'vertical',
                        }}
                        >{profile.bio}</Typography>
                      </Box>
                    </Link>
                  </RouterLink>
                </Box>
              </Grid>
            ))}
      </Grid>
    </motion.div>
    </Container>
  );
}

export default Profiles;
