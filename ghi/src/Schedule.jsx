import { useLocation } from "react-router-dom";
import React, { useState, useEffect } from "react";
import { useContextStore } from "./ContextStore";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
// import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";

import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  Box,
  Button,
  Modal,
  TextField,
} from "@mui/material";

function Schedule() {
  const {
    viewProfileDict,
    myProfileDict,
    request,
    urls,
    formatDate,
    getMyProfilePairedEvents,
    getViewedProfilePairedEvents,
    pairedEvents,
  } = useContextStore();
  const location = useLocation();

  const selectProfileID = (path) => {
    if (path === "/my_profile") {
      getMyProfilePairedEvents();
    } else {
      getViewedProfilePairedEvents();
    }
  };

  const [selectedAuthor, setSelectedAuthor] = useState("");
  const [selectedTopic, setSelectedTopic] = useState("");
  const [messageSubject, setMessageSubject] = useState("");
  const [messageBody, setMessageBody] = useState("");

  const [messageModalOpen, setMessageModalOpen] = useState(false);
  const handleSendMessageClick = (author) => {
    setSelectedAuthor(author);
    setMessageModalOpen(true);
  };
  const handleSelectedTopic = (topic) => {
    setSelectedTopic(topic);
  };
  const handleCloseMessageModal = () => setMessageModalOpen(false);

  const messageData = {
    sender: myProfileDict.profile_id,
    receiver: selectedAuthor,
    date: Date.now(),
    subject: messageSubject,
    body: messageBody,
  };

  const handlePostMessage = () => {
    handleCloseMessageModal();
    setMessageSubject("");
    setMessageBody("");
  };

  const postNewMessage = async (messageData) => {
    try {
      await request.post(urls.messages, messageData, handlePostMessage);
    } catch (error) {
      console.error("Error in sending message: ", error);
    }
  };

  const isOwner = myProfileDict.profile_id === viewProfileDict.profile_id;

  useEffect(() => {
    selectProfileID(location.pathname);
  }, [location.pathname, viewProfileDict]);

  const reset = () => {
    selectProfileID(location.pathname);
  };

  const handleExpireEvent = async (event_id) => {
    const data = {
      event_id: event_id,
    };
    const event = await request.put(urls.eventExpire(event_id), data, reset);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Box
        height="400px"
        sx={{
          p: 2,
          border: "1px solid black",
          backgroundColor: "primary.dark",
        }}
      >
        <Card
          sx={{
            p: 2,
            border: "1px solid black",
            backgroundColor: "primary.main",
            height: "100%",
            overflowY: "scroll",
          }}
        >
          <CardHeader
            title="Scheduled Appointments"
            sx={{ textAlign: "center" }}
          />

          <CardContent>
            <ul style={{ listStyleType: "none", padding: 0 }}>
              {pairedEvents?.map((event) => (
                <li key={event.event_id}>
                  <Card
                    variant="outlined"
                    sx={{ backgroundColor: "lightgray", mb: 2 }}
                  >
                    <CardHeader title={`Topic: ${event.topic}`} />
                    <CardContent>
                      <Typography variant="subtitle1">
                        Requested by: {event.author_first_name}
                      </Typography>
                      <Typography variant="subtitle1">
                        Partnered with: {event.partner_first_name}
                      </Typography>
                      <Typography variant="body1">
                        Scheduled: {formatDate(event.scheduled_at)}
                      </Typography>
                      <Typography variant="body1">
                        Topic: {event.topic}
                      </Typography>
                      <Modal
                        open={messageModalOpen}
                        onClose={handleCloseMessageModal}
                        aria-labelledby={`message-modal-${event.event_id}`}
                        aria-describedby="modal-description"
                      >
                        <Box
                          sx={{
                            position: "absolute",
                            top: "50%",
                            left: "50%",
                            transform: "translate(-50%, -50%)",
                            width: 400,
                            bgcolor: "lightgray",
                            boxShadow: 24,
                            p: 4,
                          }}
                        >
                          <h2 id="message-modal">{`Re: ${selectedTopic}`}</h2>
                          <TextField
                            label="Subject"
                            name="Subject"
                            value={messageSubject}
                            onChange={(event) =>
                              setMessageSubject(event.target.value)
                            }
                            fullWidth
                            sx={{
                              backgroundColor: "white",
                              marginBottom: 2,
                            }}
                          />
                          <TextField
                            label="Message Body"
                            name="message_body"
                            fullWidth
                            multiline
                            rows="7"
                            variant="outlined"
                            value={messageBody}
                            onChange={(event) =>
                              setMessageBody(event.target.value)
                            }
                            sx={{
                              backgroundColor: "white ",
                              marginBottom: 2,
                            }}
                          />
                          <Button
                            size="small"
                            variant="contained"
                            color="secondary"
                            onClick={() => postNewMessage(messageData)}
                            sx={{
                              marginTop: 1,
                              marginLeft: 1,
                              marginBottom: 1,
                              fontSize: "0.7rem",
                            }}
                          >
                            Send
                          </Button>
                        </Box>
                      </Modal>
                      {location.pathname === "/my_profile" ? (
                        <div

                        >
                          <Button
                            size="small"
                            variant="contained"
                            color="secondary"
                            onClick={() => {
                              handleSendMessageClick(
                                event.author === myProfileDict.profile_id
                                  ? event.partner
                                  : event.author
                              );
                              handleSelectedTopic(event.topic);
                            }}
                            sx={{
                              fontSize: "0.7rem", marginTop: 1,
                            }}
                          >
                            Send Message
                          </Button>
                          <Button
                            size="small"
                            variant="contained"
                            color="secondary"
                            onClick={() => handleExpireEvent(event.event_id)}
                            sx={{
                              fontSize: "0.7rem", marginTop: 1, marginLeft: 4,
                            }}
                          >
                            Cancel
                          </Button>
                        </div>
                      ) : (
                        <div></div>
                      )}
                    </CardContent>
                  </Card>
                </li>
              ))}
            </ul>
          </CardContent>
        </Card>
      </Box>
    </LocalizationProvider>
  );
}

export default Schedule;
