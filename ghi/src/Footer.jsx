import React from 'react';
import Box from '@mui/material/Box';
import { Container, Typography } from "@mui/material";

function Footer() {
    return (
        <Box width="100%" height="50px" bgcolor="primary.main">
        <Container>
          <Typography variant="body2" color="textSecondary" component="p">
            © 2023 2ter
          </Typography>
        </Container>
      </Box>
    );
}

export default Footer;
