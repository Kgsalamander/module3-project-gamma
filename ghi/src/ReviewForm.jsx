import React, { useState } from "react";
import {
  Typography,
  TextField,
  Button,
  Grid,
  Box,
  Modal,
  Rating,
} from "@mui/material";
import { useContextStore } from "./ContextStore";

function ReviewForm({ handleClose }) {
  const {
    myProfileDict,
    viewProfileDict,
    request,
    getReviewsByReviewee,
    urls,
  } = useContextStore();

  const [rating, setRating] = useState(1);
  const [reviewText, setReviewText] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();

    const newReview = {
      author: myProfileDict.profile_id,
      reviewee: viewProfileDict.profile_id,
      date: new Date().toISOString(),
      rating: rating,
      review_text: reviewText,
    };

    try {
      await request.post(urls.reviews, newReview, () => {
        getReviewsByReviewee();
        handleClose();
      });
    } catch (error) {
      console.error("Error submitting review:", error);
    }
  };

  return (
    <Modal open={true} onClose={handleClose}>
      <Box
        width={400}
        bgcolor="background.paper"
        p={2}
        sx={{
          backgroundColor: "primary.dark",
          zIndex: 1600,
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
        }}
      >
        <Typography variant="h6">Submit a Review</Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography>Rating</Typography>
              <Rating
                name="rating"
                value={rating}
                onChange={(event, newValue) => {
                  setRating(newValue);
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                label="Review Text"
                multiline
                rows={4}
                variant="outlined"
                fullWidth
                value={reviewText}
                onChange={(e) => setReviewText(e.target.value)}
                required
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                size="small"
                variant="contained"
                color="secondary"
                type="submit"
                sx={{ margin: 1, fontSize: "0.7rem" }}
              >
                Submit Review
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}

export default ReviewForm;
