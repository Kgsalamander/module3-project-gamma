import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState } from "react";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import SignUpForm from "./SignUpForm";
import LoginForm from "./LoginForm";
import Nav from "./Nav";
import Footer from "./Footer";
import "./App.css";
import Me from "./Me";
import Profiles from "./Profiles";
import ViewProfile from "./ViewProfile";
import Messages from "./Messages";
import Main, {theme} from "./main"; // do not delete Main
import Box from '@mui/material/Box';
import { ThemeProvider } from '@mui/material/styles';
import background from './assets/background.jpg';

function App() {
  const [userData] = useState("");
  const domain = /https?:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');
  const baseUrl = process.env.REACT_APP_API_HOST

  return (
    <ThemeProvider theme={theme}>
      {/* os.environ */}
      <AuthProvider baseUrl={baseUrl}> 
        <BrowserRouter basename={basename}>
          <Nav userData={userData} />
          <Box sx={{ p: 2, border: '1px solid black',
              backgroundColor: "#DCDCDC",
              backgroundImage: `url(${background})`,
              backgroundSize: 'cover',
              backgroundPosition: 'center',
              minHeight: '100vh'}}>
            <Routes>
              <Route path="/" element={<Profiles />} />
              <Route path="/login" element={<LoginForm />} />
              <Route path="/signup" element={<SignUpForm />} />
              <Route path="/profiles" element={<Profiles />} />
              <Route path="/my_profile" element={<Me />} />
              <Route path="/view_profile" element={<ViewProfile />} />
              <Route path="/messages" element={<Messages />} />
            </Routes>
            </Box>
          <Footer />
        </BrowserRouter>
      </AuthProvider>
    </ThemeProvider>
  );
}

export default App;
