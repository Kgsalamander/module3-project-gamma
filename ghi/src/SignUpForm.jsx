import React, { useState } from "react";
import { motion } from "framer-motion";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useContextStore } from "./ContextStore";

function SignUpForm() {
  const { getAccountByLoginUsername } = useContextStore();
  const baseUrl = process.env.REACT_APP_API_HOST

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  // const { login } = useToken();
  const navigate = useNavigate();

  const handleEmailChange = (e) => {
    const value = e.target.value;
    setEmail(value);
  };

  const handleUserNameChange = (e) => {
    const value = e.target.value;
    setUserName(value);
  };

  const handlePasswordChange = (e) => {
    const value = e.target.value;
    setPassword(value);
  };

  const handleFirstNameChange = (e) => {
    const value = e.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = (e) => {
    const value = e.target.value;
    setLastName(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const accountData = {
      account_id: 0,
      email: email,
      username: username,
      password: password,
      first_name: firstName,
      last_name: lastName,
    };

    try {
      const signupResponse = await fetch(`${baseUrl}/api/accounts`, {
        method: "POST",
        body: JSON.stringify(accountData),
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (signupResponse.ok) {
        getAccountByLoginUsername(username);
        e.target.reset();
        navigate("/login");
      } else {
        console.error("Signup failed");
      }
    } catch (error) {
      console.error("Error during signup:", error);
    }
  };

  return (
    <motion.div
      className="row"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <div className="offset-3 col-6">
        <div className="card">
          <h1 className="offset-3">Sign Up</h1>
          <form
            className="form"
            onSubmit={(e) => handleSubmit(e)}
            id="create-account-form"
          >
            <div className="form-floating mb-3">
              <input
                value={email}
                onChange={handleEmailChange}
                placeholder="email"
                required
                type="text"
                name="email"
                id="email"
                className="form-control"
              />
              <label htmlFor="email">Email:</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={username}
                onChange={handleUserNameChange}
                placeholder="username"
                required
                type="text"
                name="username"
                id="username"
                className="form-control"
              />
              <label htmlFor="username">Username:</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={password}
                onChange={handlePasswordChange}
                placeholder="password"
                required
                type="password"
                name="password"
                id="password"
                className="form-control"
              />
              <label htmlFor="password">Password:</label>
            </div>

            <div className="form-floating mb-3">
              <input
                value={firstName}
                onChange={handleFirstNameChange}
                placeholder="first name"
                required
                type="firstName"
                name="firstName"
                id="firstName"
                className="form-control"
              />
              <label htmlFor="firstName">First Name:</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={lastName}
                onChange={handleLastNameChange}
                placeholder="lat name"
                required
                type="lastName"
                name="lastName"
                id="lastName"
                className="form-control"
              />
              <label htmlFor="lastName">Last Name:</label>
            </div>

            <button className="btn btn-primary">Sign Up</button>
          </form>
        </div>
      </div>
    </motion.div>
  );
}

export default SignUpForm;
